
## Install npm modules
`Run npm install to install required dependencies.`

## How to run the server?

```
> node server.js
> nodemon server.js to start the nodemon server
> mongod to start the mongoDB shell

```

## How to access the application 

```
POST : localhost:7000/api/v1/Register
POST : localhost:7000/api/v1/login

POST : localhost:7000/api/v1/Add-user
GET : localhost:7000/api/v1/allUsers
PUT : localhost:7000/api/v1/user/:id
DELETE : localhost:7000/api/v1/user/:id

```
