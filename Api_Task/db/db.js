const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Api-task', {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})
.then(() => {console.log('DB connected successfully')})
.catch((err) => {console.log('Failed to connect DB')})
