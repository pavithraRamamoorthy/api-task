const mongoose = require('mongoose');
  const { Schema } = mongoose;

  const loginschema = new Schema({
    FirstName: String,
    LastName: String,
    MobileNo: String,
    email: {
      type: String,
      unique: true
    },
    password: String,
  });

  const Login = mongoose.model('login', loginschema);

  module.exports = Login;