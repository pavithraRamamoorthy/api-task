const mongoose = require('mongoose');
  const { Schema } = mongoose;

  const userschema = new Schema({
    Name : String,
    PhoneNo: String,
    Address : String,
    State : String,
    City : String,
  },
  
  { collection: 'user'});

  const User = mongoose.model('user', userschema);

  module.exports = User;