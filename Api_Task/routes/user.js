const express = require('express');
const loginUser = require('../controllers/register');
const addUser = require('../controllers/Add_user');
const getUser = require('../controllers/user_list');
const updateUser = require('../controllers/Edit_user');
const deleteUser = require('../controllers/Delete_user');
const { update } = require('../model/login_model');


const router = express.Router();

router.route('/register')
.post(loginUser.RegisterUser)

router.route('/login')
.post(loginUser.Login)


router.route('/Add-user')
.post(addUser.createUser)

router.route('/allUsers')
.get(getUser.getAllUsers)

router.route('/')
.get(getUser.getuser)

router.route('/user/:id')
.put(updateUser.updateUserData)
.delete(deleteUser.deleteUser)


module.exports = router;