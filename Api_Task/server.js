const express = require('express');
const config = require('config');
const routes = require('./routes/user');
const bodyParser = require('body-parser');

require('./db/db');

const app = express();
app.use(express.static('./views'));
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));  

app.set("view engine", "ejs");

app.use('/api/v1', routes);



const PORT = config.get('PORT');

app.listen(PORT, () => {console.log(`Server listening port ${PORT}`)});
