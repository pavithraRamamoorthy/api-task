// 4)Delete User - DELETE

const User = require('../model/user_model');

const deleteUser = async (req, res) => {
    try{

        const id  = req.params.id;
        await User.findOneAndRemove({id});
        if (!id) {
            res.status(500).json({ message: 'User not found' });
        }
        res.json({ message: `user ${id} deleted.` });
        }catch (error) {
            console.log(error);
            res.status(500).json({ message: 'Failed to delete' });
        }
    }


module.exports = { deleteUser  };