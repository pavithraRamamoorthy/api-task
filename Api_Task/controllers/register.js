// 1)Login Form - POST
//   Fields:
//     Username
//     Password
//   Return result as token


const Register = require('../model/login_model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const RegisterUser = async (req, res, next) => {
    try {
        const pass = await bcrypt.hash(req.body.password, 12);
        const data = {...req.body, password: pass};
        const a = await Register.create(data);
        res.json(a);
    } catch (error) {
        console.log(error);
        res.status(500).json({message: 'Failed to Register'});
    }
};

const Login = async (req, res, next) => {
    const username = req.body.email;
    const password = req.body.password;

    const user = await Register.findOne({email: username});
    if(!user) {
        res.status(500).json({message: 'User Not Found'});
    }
    const passwordCheck = await bcrypt.compare(password, user.password);
    if(!passwordCheck) {
        res.status(500).json({message: 'Ur credentials are wrong'});
    }
    const token = await jwt.sign({ email: user.email }, 'secret');
    res.json({token: token});
};

module.exports = { RegisterUser , Login };