// 2)User List - GET
//     List the user in table



const User = require('../model/user_model');

const getAllUsers = async (req, res, next) => {
    try {
        await User.find({}, function (err, allDetails) {
            if (err) {
                console.log(err);
            } else {
                res.render("index", { details: allDetails })
            }
        } );
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Failed to get Data' });
    }
};



const getuser =  async (req, res) => {
   await res.render("index",{ details: null })
}
module.exports = {  getAllUsers, getuser };
