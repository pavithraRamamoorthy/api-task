// 3)Add User - POST
//   Fields
//     Name
//     Phone Number
//     Address
//     State
//     City


const User = require('../model/user_model');

const createUser = async (req, res, next) => {
    try {
        const data = { ...req.body};
        const a = await User.create(data);
        res.json(a);
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Failed to Create' });
    }
};



module.exports = { createUser  };