// 4)Edit User - PUT
//     Fields
//     Name
//     Phone Number
//     Address
//     State
//     City

const User = require('../model/user_model');

const updateUserData = async (req, res, next) => {
  let { ...data } = req.body;
  const result = await User.findOneAndUpdate(
    { _id: req.params.id },
    data,
    {
      new: true,
    }
  );

  res.send(result);
};


module.exports = {  updateUserData };